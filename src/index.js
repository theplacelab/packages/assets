import DropAreaComponent from "./components/DropArea";
import DropAssetComponent from "./components/DropAsset";
import AssetSelectorComponent from "./components/AssetSelector";
import TypeIcon from "./components/TypeIcon";
import store from "./state/store.js";
import { DndProvider } from "react-dnd-multi-backend";
import HTML5toTouch from "react-dnd-multi-backend";
import React from "react";
import { Provider } from "react-redux";
import { assetDelete, assetUpsert } from "./actions";

const DropArea = (props) => {
  return (
    <DndProvider options={HTML5toTouch}>
      <Provider store={store}>
        <DropAreaComponent {...props} />
      </Provider>
    </DndProvider>
  );
};

const AssetSelector = (props) => {
  return <AssetSelectorComponent {...props} />;
};

const DropAsset = (props) => {
  return (
    <DndProvider options={HTML5toTouch}>
      <Provider store={store}>
        <DropAssetComponent {...props} />
      </Provider>
    </DndProvider>
  );
};

const asset = {
  upsert: ({ data, onComplete }) => {
    store.dispatch(
      assetUpsert({
        ...data,
        onComplete,
        currentAsset: null,
        instanceId: null,
      })
    );
  },

  destroy: ({ asset, onComplete }) => {
    store.dispatch(assetDelete({ id: asset.id, onComplete }));
  },
};

export { TypeIcon, DropArea, DropAsset, AssetSelector, asset };
