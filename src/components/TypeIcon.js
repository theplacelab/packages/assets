import React from "react";

export const TypeIcon = ({ type, showLabel = true, style, onClick }) => {
  const styles = {
    iconRow: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    icon: { color: "blue", fontSize: "1rem", ...style?.icon },
    label: {
      marginLeft: ".4rem",
      fontSize: "1rem",
      fontFace: "sans",
      fontWeight: 900,
      ...style?.label,
    },
  };
  switch (type) {
    case "image/png":
    case "image/jpg":
    case "image/jpeg":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div
            title={type}
            style={styles.icon}
            className={"far fa-file-image"}
          />
          {showLabel && <div style={styles.label}>Image</div>}
        </div>
      );
    case "application/pdf":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div title={type} style={styles.icon} className={"far fa-file-pdf"} />
          {showLabel && <div style={styles.label}>PDF</div>}
        </div>
      );
    case "audio/wav":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div
            title={type}
            style={styles.icon}
            className={"far fa-file-audio"}
          />
          {showLabel && <div style={styles.label}>Audio</div>}
        </div>
      );
    case "video/mp4":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div
            title={type}
            style={styles.icon}
            className={"far fa-file-video"}
          />
          {showLabel && <div style={styles.label}>Video</div>}
        </div>
      );
    case "text/plain":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div title={type} style={styles.icon} className={"far fa-file-alt"} />
          {showLabel && <div style={styles.label}>Video</div>}
        </div>
      );
    case "custom/vimeo":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div title={type} style={styles.icon} className={"fab fa-vimeo"} />
          {showLabel && <div style={styles.label}>Vimeo Embed</div>}
        </div>
      );
    case "custom/youtube":
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div title={type} style={styles.icon} className={"fab fa-youtube"} />
          {showLabel && <div style={styles.label}>YouTube Embed</div>}
        </div>
      );
    default:
      return (
        <div style={styles.iconRow} onClick={onClick}>
          <div title={type} style={styles.icon} className={"far fa-file"} />
          {showLabel && <div style={styles.label}>Unknown</div>}
        </div>
      );
  }
};
export default TypeIcon;
