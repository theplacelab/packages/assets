import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import PropTypes from 'prop-types';

class DropZone extends Component {
  render() {
    const isActive = this.props.canDrop && this.props.isOver;
    const dropTarget = this.props.connectDropTarget(
      <div
        onContextMenu={this.props.onContextMenu}
        {...this.props.dataset}
        style={this.props.style}
        onClick={this.onClick}
        className={
          isActive
            ? this.props.style.dropZone_active
            : this.props.style.dropZone
        }
      >
        {this.props.children}
      </div>
    );
    return <React.Fragment>{dropTarget}</React.Fragment>;
  }
}

DropZone.propTypes = {
  onContextMenu: PropTypes.func,
  style: PropTypes.object,
  hasButton: PropTypes.bool,
  previewImage: PropTypes.string,
  children: PropTypes.node,
  onDrop: PropTypes.func,
  style: PropTypes.object,
  dropMessage: PropTypes.string,
  allowClickToZoom: PropTypes.bool,
  dataset: PropTypes.object,
  connectDropTarget: PropTypes.func,
  canDrop: PropTypes.bool,
  isOver: PropTypes.bool
};

export default DropTarget(
  (props) => props.accepts,
  {
    drop(props, monitor) {
      if (props.onDrop) {
        props.onDrop(props, monitor);
      }
    }
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  })
)(DropZone);
