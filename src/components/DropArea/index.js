import React, { useMemo, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { NativeTypes } from 'react-dnd-html5-backend';
import DropZone from './DropZone';
import styles from './DropArea.module.css';
const { FILE } = NativeTypes;
import { assetUpsert, assetDelete } from '../../actions.js';
import { config } from '../../actions.js';
import uuid from 'react-uuid';

const DropArea = (props) => {
  const dispatch = useDispatch();
  const {
    token,
    accepts,
    serviceUrl,
    onContextMenu,
    onDrop,
    id,
    onComplete,
    meta,
    style
  } = props;
  const { skipService } = props.options
    ? props.options
    : { skipService: false };
  const assetServiceReachable = useSelector(
    (redux) => redux.assetServiceReachable
  );
  const [instanceId] = useState(id ? id : uuid());
  let isBusy = useSelector((redux) => redux[instanceId]?.isBusy);
  isBusy = isBusy === true ? true : false;
  const isServiceReachable =
    skipService || useSelector((redux) => redux.isReachable);

  const handleDrop = onDrop
    ? onDrop
    : (e) => {
        if (isBusy) return;
        const file = e.files[0];
        const handleComplete = (file) => {
          if (typeof onComplete === 'function') onComplete(file);
        };
        const reader = new FileReader();
        reader.readAsDataURL(file);
        dispatch(
          assetUpsert({
            meta,
            file,
            token,
            onComplete: handleComplete,
            instanceId
          })
        );
      };

  // Configure services
  useEffect(() => {
    if (!skipService)
      dispatch(
        config({
          token,
          serviceUrl,
          onComplete
        })
      );
  }, [serviceUrl, token, onComplete]);

  return (
    <div style={{ ...style }}>
      <DropZone
        {...props}
        style={styles}
        onContextMenu={onContextMenu}
        accepts={isServiceReachable ? FILE : []}
        onDrop={(item, monitor) => {
          if (monitor) handleDrop({ files: monitor.getItem().files });
        }}
      />
    </div>
  );
};
export default DropArea;

DropArea.defaultProps = {
  allowClickToZoom: false,
  children: null
};
