import React, { useRef, useState, useEffect } from 'react';
import DropArea from '../DropArea/index.js';
import { useSelector, useDispatch } from 'react-redux';
import { assetUpsert, assetDelete } from '../../actions.js';
import uuid from 'react-uuid';

const ProgressBar = ({ top, width, percentComplete, style }) => {
  return (
    <div
      style={{
        position: 'absolute',
        width,
        height: '2rem'
      }}
    >
      <div
        style={{
          zIndex: 1,
          position: 'absolute',
          width: '100%',
          top,
          left: 0,
          background: `linear-gradient(90deg, rgb(204 0 255) 0%, rgba(0,0,0,0) ${percentComplete})`,
          height: '1rem'
        }}
      />
    </div>
  );
};

const DropAsset = ({
  hasDelete = false,
  src,
  serviceUrl,
  style,
  token,
  onComplete,
  onDelete,
  options = {
    height: 200,
    width: 200,
    zoom: false,
    replace: false,
    skipService: false
  },
  id,
  meta
}) => {
  const dispatch = useDispatch();
  const [instanceId] = useState(id ? id : uuid());
  const isServiceReachable = useSelector((redux) => redux.isReachable);
  let isBusy = useSelector((redux) => redux[instanceId]?.isBusy);
  isBusy = isBusy === true ? true : false;
  const [hover, setHover] = useState(false);
  const [imgMissing, setImgMissing] = useState(true);
  const [droppedFile, setDroppedFile] = useState(null);
  const [currentAsset, setCurrentAsset] = useState(
    src?.replace(/^.*[\\\/]/, '')
  );
  const percentComplete = useSelector(
    (redux) => redux[instanceId]?.percentComplete
  );
  const img = useRef();
  const { height, width, zoom, replace, skipService = false } = options;
  const handleDrop = (e) => {
    if (isBusy) return;
    const file = e.files[0];
    const handleComplete = (file) => {
      setCurrentAsset(file.fileName);
      if (typeof onComplete === 'function') onComplete(file);
    };
    setDroppedFile(file);
    setImgMissing(false);
    img.current.file = file;
    const reader = new FileReader();
    reader.onload = ((aImg) => (e) => {
      aImg.src = e.target.result;
      img.current.style.display = 'block';
    })(img.current);
    reader.readAsDataURL(file);
    if (!skipService) {
      dispatch(
        assetUpsert({
          replace: currentAsset,
          meta,
          file,
          token,
          onComplete: handleComplete,
          currentAsset,
          instanceId
        })
      );
    } else {
      handleComplete(file);
    }
  };

  let dropMessage;
  if (droppedFile?.type) {
    dropMessage = (
      <div>
        <div
          style={{ color: 'white' }}
          className={`fas ${
            droppedFile.type === 'application/pdf' ? 'fa-file-pdf' : 'fa-file'
          }`}
        />
        <div style={{ color: 'black', fontSize: '.5rem' }}>
          {droppedFile.name}
        </div>
      </div>
    );
  } else {
    dropMessage = <div className={hover ? 'far fa-plus-square' : ''} />;
  }

  if (!isServiceReachable)
    dropMessage = (
      <div
        title="Asset Server Offline"
        className={'fas fa-exclamation-triangle'}
        style={{
          zIndex: 1,
          color: '#ffc722',
          fontSize: '2rem'
        }}
      />
    );

  dropMessage = (
    <div
      style={{
        display: 'flex',
        position: 'absolute',
        width,
        height,
        alignContent: 'center',
        alignItems: 'center',
        color: 'darkgray',
        fontSize: `${height * 0.25}px`,
        overflow: 'clip',
        topMargin: `${height - height * 0.25}px`
      }}
    >
      <div style={{ margin: 'auto' }}>{dropMessage}</div>
    </div>
  );
  const deleteAvailable =
    hasDelete && hover && !isBusy && !imgMissing && !skipService;
  return (
    <div
      style={{ width, height, overflow: 'hidden', ...style }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      {deleteAvailable && (
        <div
          onClick={() => {
            img.current.src = '';
            setDroppedFile(null);
            dispatch(assetDelete({ id: currentAsset, onComplete: onDelete }));
          }}
          title="Delete File"
          className={'fas fa-trash'}
          style={{
            zIndex: 1,
            position: 'absolute',
            color: 'black',
            margin: '.2rem',
            fontSize: '1.5rem',
            left: `${width - 15}px`
          }}
        />
      )}
      <DropArea
        replace={true}
        token={token}
        id={instanceId}
        options={options}
        serviceUrl={serviceUrl}
        onDrop={handleDrop}
      >
        <div
          style={{
            display: 'flex',
            height,
            width,
            background: 'grey'
          }}
        >
          {dropMessage}
          {isBusy && (
            <ProgressBar
              percentComplete={`${percentComplete}%`}
              width={width}
            />
          )}
          <img
            onError={(e) => {
              e.target.style.display = 'none';
              setImgMissing(true);
            }}
            onLoad={(e) => setImgMissing(false)}
            draggable="false"
            alt=""
            ref={img}
            src={src} //FIXME: Breaks drop`${src}?cachebust=${uuid()}`
            style={{
              userSelect: 'none',
              objectFit: 'cover',
              height,
              width,
              filter: isBusy
                ? `grayscale(${100 - percentComplete}%)`
                : 'grayscale(0%)'
            }}
          />
        </div>
      </DropArea>
    </div>
  );
};
export default DropAsset;
