import { put, take, select } from 'redux-saga/effects';
import { channel } from 'redux-saga';
import * as actions from '../../actions.js';

const progressChannel = channel();
export function* watchProgressChannel() {
  while (true) {
    const action = yield take(progressChannel);
    yield put(action);
  }
}

export const validateConfig = function* (action) {
  const { serviceUrl, token } = action.payload;
  if (!serviceUrl) {
    console.error(
      '@theplacelab/assets: Missing config. You must provide reachable serviceUrl OR skipService:true'
    );
    yield put({
      type: actions.CONFIG_VALIDATION_RESPONSE,
      payload: { serviceUrl, isReachable: false }
    });
    return;
  } else {
    const checkServiceUrl = yield _makeRequest({
      method: 'GET',
      url: `${serviceUrl}/up`
    });
    if (checkServiceUrl.ok)
      yield put({
        type: actions.CONFIG_VALIDATION_RESPONSE,
        payload: { token, serviceUrl, isReachable: true }
      });
  }
};

export const add = function* (action) {
  const reduxState = yield select();
  yield put({
    type: actions.IS_BUSY,
    payload: { instanceId: action.payload.instanceId, value: true }
  });
  yield put({
    type: actions.PERCENT_COMPLETE,
    payload: { instanceId: action.payload.instanceId, progress: 0 }
  });
  try {
    if (action.payload.file) {
      yield _addAssetWithFile(
        action.payload.instanceId,
        action.payload.file,
        reduxState.token,
        (registry) => {
          if (typeof action.payload.onComplete === 'function')
            action.payload.onComplete({
              registry,
              file: action.payload.file,
              replaces: action.payload.replace
            });
        },
        (e) => console.error(e),
        (progress) =>
          progressChannel.put({
            type: actions.PERCENT_COMPLETE,
            payload: { instanceId: action.payload.instanceId, progress }
          }),
        action.payload.meta,
        action.payload.replace
      );
    } else {
      yield _addAsset(action.payload, (e) => {
        if (typeof action.payload.onComplete === 'function') {
          action.payload.onComplete();
        }
      });
    }
  } catch (e) {
    yield put({
      type: actions.SAGA_ERROR,
      payload: {
        error: e,
        source: action.type
      }
    });
    progressChannel.put({
      type: actions.IS_BUSY,
      payload: { instanceId: action.payload.instanceId, value: false }
    });
  }
};

export const destroy = function* (action) {
  if (!action.payload.id) return;
  try {
    const reduxState = yield select();
    const response = yield _makeRequest({
      method: 'DELETE',
      url: `${reduxState.serviceUrl}/asset/${action.payload.id}`,
      token: reduxState.token
    });
    if (response.ok) {
      if (typeof action.payload.onComplete === 'function')
        action.payload.onComplete();
    } else {
      yield put({
        type: actions.SAGA_ERROR,
        payload: {
          error: response,
          source: action.type
        }
      });
    }
  } catch (e) {
    yield put({
      type: actions.SAGA_ERROR,
      payload: {
        error: e,
        source: action.type
      }
    });
  }
};

// Add or update asset with NO file (updates if has data.id)
const _addAsset = function* (data, cb) {
  if (!data.id) {
    const reduxState = yield select();
    const xhr = new XMLHttpRequest();
    const requestURL = `${reduxState.serviceUrl}/asset`;
    xhr.open('PUT', requestURL);
    xhr.setRequestHeader('Authorization', 'BEARER ' + reduxState.token);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.onerror = (e) => {
      console.error(requestURL);
      console.error(e);
    };
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 201) {
          if (typeof cb === 'function') cb();
        }
      }
    };
    xhr.send(JSON.stringify(data));
  } else {
    // Update
    const reduxState = yield select();
    const xhr = new XMLHttpRequest();
    const requestURL = `${reduxState.serviceUrl}/asset/${data.id}`;
    xhr.open('PATCH', requestURL);
    xhr.setRequestHeader('Authorization', 'BEARER ' + reduxState.token);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.onerror = (e) => {
      console.error(requestURL);
      console.error(e);
    };
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (typeof cb === 'function') cb();
        }
      }
    };
    xhr.send(JSON.stringify(data));
  }
};

// Add or update asset WITH a file (updates if has data.id)
const _addAssetWithFile = function* (
  instanceId,
  file,
  authToken,
  onSuccess,
  onFail,
  onProgress,
  meta,
  replace
) {
  const reduxState = yield select();
  const xhr = new XMLHttpRequest();
  const requestURL = replace
    ? `${reduxState.serviceUrl}/asset/replace/${replace}`
    : `${reduxState.serviceUrl}/asset`;
  xhr.open('PUT', requestURL);
  xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  xhr.setRequestHeader('Authorization', 'BEARER ' + authToken);
  xhr.onerror = (e) => {
    console.error(requestURL);
    console.error(e);
  };
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 201) {
        const registryEntry = JSON.parse(xhr.responseText);
        _uploadToS3({
          instanceId,
          file,
          signedRequest: registryEntry.signedRequest,
          onSuccess: () => onSuccess(registryEntry),
          onFail,
          onProgress
        });
      } else {
        console.error(xhr);
        console.error(
          'ERROR: Failed to get signed URL. Check asset service logs for more information.'
        );
        progressChannel.put({
          type: actions.IS_BUSY,
          payload: { instanceId, value: false }
        });
      }
    }
  };
  xhr.send(
    JSON.stringify({
      asset_path: file.name,
      asset_type: file.type,
      asset_metadata: meta
    })
  );
};

const _uploadToS3 = ({
  instanceId,
  file,
  signedRequest,
  onSuccess,
  onFail,
  onProgress
}) => {
  const xhr = new XMLHttpRequest();
  xhr.upload.onprogress = (e) => {
    if (e.lengthComputable) onProgress(Math.ceil((e.loaded / e.total) * 100));
  };
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        onSuccess();
        onProgress(0);
        progressChannel.put({
          type: actions.IS_BUSY,
          payload: { instanceId, value: false }
        });
      } else {
        console.error(xhr);
        onFail('Could not upload file!');
        onProgress(0);
        progressChannel.put({
          type: actions.IS_BUSY,
          payload: { instanceId, value: false }
        });
      }
    }
  };
  xhr.open('PUT', signedRequest);
  xhr.send(file);
};

const _makeRequest = function* ({ method, url, body, token }) {
  try {
    let headers = {
      Accept: 'application/json, application/xml, text/plain, text/html, *.*',
      'Content-Type': 'application/json; charset=utf-8'
    };
    if (token) headers = { ...headers, Authorization: 'Bearer ' + token };
    let request = {
      method: method,
      headers,
      body: body
        ? typeof body === 'string'
          ? body
          : JSON.stringify(body).replace(/\\u0000/g, '')
        : null
    };
    if (!body || method === 'GET') delete request.body;
    return yield fetch(url, request);
  } catch (e) {
    yield put({ type: actions.SAGA_ERROR });
    console.error('@theplacelab/assets: Cannot reach asset helper endpoint!');
    return e;
  }
};
