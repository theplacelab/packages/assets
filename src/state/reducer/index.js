import * as actions from "../../actions";

const reducer = (
  state = {
    serviceUrl: null,
    isReachable: false,
    isBusy: false,
  },
  action
) => {
  switch (action.type) {
    case actions.CONFIG_VALIDATION_RESPONSE:
      return { ...state, ...action.payload };
    case actions.IS_BUSY:
      return {
        ...state,
        [action.payload.instanceId]: {
          ...state[action.payload.instanceId],
          isBusy: action.payload.value,
        },
      };
    case actions.PERCENT_COMPLETE:
      return {
        ...state,
        [action.payload.instanceId]: {
          ...state[action.payload.instanceId],
          percentComplete: action.payload.progress,
        },
      };
    default:
      return state;
  }
};

export default reducer;
