import { takeLatest, all, call } from "redux-saga/effects";
import * as actions from "../actions";
import * as asset from "./saga/asset.js";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducer";
import { createStore, applyMiddleware } from "redux";

function* rootSaga() {
  yield all([
    asset.watchProgressChannel(),
    takeLatest(actions.ASSET_UPSERT, asset.add),
    takeLatest(actions.ASSET_DELETE, asset.destroy),
    takeLatest(actions.SAGA_ERROR, (e) => {
      console.error(e);
    }),
    takeLatest(actions.CONFIG, asset.validateConfig),
  ]);
}

const sagaMiddleware = createSagaMiddleware();
const store = () => {
  let store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(rootSaga);
  return store;
};

export default store();
