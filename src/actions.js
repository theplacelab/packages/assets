function action(type, payload = {}) {
  return { type, payload };
}

export const ASSET_UPSERT = "ASSET_UPSERT";
export const ASSET_DELETE = "ASSET_DELETE";
export const assetUpsert = (payload) => action(ASSET_UPSERT, payload);
export const assetDelete = (payload) => action(ASSET_DELETE, payload);

export const ASSET_SIGNED = "ASSET_SIGNED";
export const ASSET_SIGNED_RESPONSE = "ASSET_SIGNED_RESPONSE";
export const ASSET_DELETE_RESPONSE = "ASSET_DELETE_RESPONSE";

export const SAGA_ERROR = "SAGA_ERROR";
export const CONFIG = "CONFIG";
export const CONFIG_VALIDATION_RESPONSE = "CONFIG_VALIDATION_RESPONSE";
export const IS_BUSY = "IS_BUSY";
export const PERCENT_COMPLETE = "PERCENT_COMPLETE";
export const config = (payload) => action(CONFIG, payload);
export const isUploading = (payload) => action(IS_BUSY, payload);
export const percentComplete = (payload) => action(PERCENT_COMPLETE, payload);
