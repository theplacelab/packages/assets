# Package: @theplacelab/assets

**v. ALPHA**  
React client support for [Asset Service](https://gitlab.com/theplacelab/service/assets).

[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/auth/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/auth/-/pipelines/latest)

<img width="500" src="https://assets.feralresearch.org/demo/Screen%20Shot%202022-01-10%20at%202.26.15%20PM.png">
<img width="800" src="https://assets.feralresearch.org/demo/Screen%20Shot%202022-01-10%20at%202.25.09%20PM.png">

# In This Package:

## `<DropArea>`

Basic dropzone, supports any content, provides upload capability with a callback.

```js
<DropArea serviceUrl="" style="" token="" onComplete="" options="" />
```

## `<DropAsset>`

Wraps `<DropArea>` with support for media preview, providing an asset-store backed droppable image.

```js
<DropAsset
  src=""
  serviceUrl=""
  style=""
  token=""
  onComplete=""
  options=""
  id=""
  meta=""
/>
```

## `<TypeIcon>`

Displays a nicely styled icon give an asset mimetype.

```js
<TypeIcon type="" showLabel="" style="" onClick="" />
```

## `Asset Helper`

The exported `asset` object contains helper methods (very WiP right now)

### `upsert: ({ data, onComplete })`

Programatically create/update an asset, for building your own UI when `<DropAsset/>` isn't enough.

### `destroy: ({ data, onComplete })`

Programatically destroy an asset.

&nbsp;

---

&nbsp;

# Auto-Deployment Using Semantic Release

- [[README: Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)]

# Development

Use [yalc](https://github.com/wclr/yalc) to [solve issues with link](https://divotion.com/blog/yalc-npm-link-alternative-that-does-work). Install yalc, then:

In this package:

```
yalc publish
yalc push
```

Where you want to use it:

```
yalc link @theplacelab/assets
```

# How to Use

- Get a Personal Access Token from Gitlab

- Add an `.npmrc` to your project using the token in place of ~GITLAB_TOKEN~ (or alternately add this to your ~/.npmrc or use config to do that)

  ```
  @theplacelab:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken="~GITLAB_TOKEN~"
  ```

  or

  ```
  npm config set @theplacelab:registry https://gitlab.com/api/v4/packages/npm/
  npm config set //gitlab.com/api/v4/packages/npm/:_authToken '~GITLAB_TOKEN~'
  ```

- `yarn add @theplacelab/s3`
