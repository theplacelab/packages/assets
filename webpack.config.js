var path = require("path");
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const WebpackShellPlugin = require("webpack-shell-plugin");

module.exports = (env, argv) => {
  const mode = argv.mode;
  const postBuildScript =
    process.env.YALC === "true" ? "npx yalc push" : "true";
  let plugins = argv.addon === "analyze" ? [new BundleAnalyzerPlugin()] : [];
  plugins.push(new WebpackShellPlugin({ onBuildExit: postBuildScript }));

  console.log(`Environment: ${mode}`);
  return {
    plugins,
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "[name].js",
      library: "index",
      globalObject: "this",
      libraryTarget: "commonjs2",
    },
    externals: {
      react: "commonjs react",
      "@theplacelab/ui": "commonjs @theplacelab/ui",
      "prop-types": "commonjs prop-types",
      "@fortawesome/react-fontawesome":
        "commonjs @fortawesome/react-fontawesome",
      "@fortawesome/free-solid-svg-icons":
        "commonjs @fortawesome/free-solid-svg-icons",
      "@fortawesome/fontawesome-svg-core":
        "commonjs @fortawesome/fontawesome-svg-core",
      "react-redux": "commonjs react-redux",
      "react-dom": "commonjs react-dom",
      "react-router-dom": "commonjs react-router-dom",
      "redux-saga": "commonjs redux-saga",
      "react-dnd": "commonjs react-dnd",
      "react-dnd-html5-backend": "commonjs react-dnd-html5-backend",
      "react-dnd-multi-backend": "commonjs react-dnd-multi-backend",
      "react-dnd-touch-backend": "commonjs react-dnd-touch-backend",
    },
    devtool: mode === "development" ? "inline-source-map" : false,
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.join(__dirname, "src"),
          exclude: path.join(__dirname, "/node_modules/"),
          loader: "babel-loader",
        },
        {
          test: /\.css$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: {
                importLoaders: 1,
                modules: true,
              },
            },
          ],
          include: /\.module\.css$/,
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
          exclude: /\.module\.css$/,
        },
      ],
    },
    /*optimization: {
      runtimeChunk: "single",
      splitChunks: {
        chunks: "all",
        maxInitialRequests: Infinity,
        minSize: 0,
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1];

              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace("@", "")}`;
            },
          },
        },
      },
    },*/
  };
};
